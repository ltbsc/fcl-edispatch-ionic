import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Loading } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { TruckListPage } from '../truck-list/truck-list';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
  providers: [AuthProvider]
})
export class LoginPage {

  username: string
  password: string
  mobileNumber: string

  private loading: Loading

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public auth: AuthProvider,
    public loadingCtrl: LoadingController
  ) {
    this.username = null
    this.password = null
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }


  /**
   * 
   */
  attemptLogin() {
    this.showLoader()
    this.auth.login('user1@trucker.com', 'user1', 'sample string 3')
      .then(() => {
        this.navCtrl.setRoot(TruckListPage)
      })
      .catch(err => {
        this.dimissLoader()
      })
  }


  /**
   * Show the loading overlay
   */
  showLoader() {
    this.loading = this.loadingCtrl.create({
      content: 'Loggin in...',
      dismissOnPageChange: true
    })

    this.loading.present()
  }

  /**
   * Dismiss the loading overlay
   */
  dimissLoader() {
    this.loading.dismiss()
  }

}
