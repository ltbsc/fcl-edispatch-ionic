import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TruckListPage } from './truck-list';
import { SelectDriverPageModule } from '../select-driver/select-driver.module';

@NgModule({
  declarations: [
    TruckListPage,
  ],
  imports: [
    IonicPageModule.forChild(TruckListPage),
    SelectDriverPageModule
  ],
})
export class TruckListPageModule {}
