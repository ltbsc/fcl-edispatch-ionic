import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { TrucksProvider } from '../../providers/trucks/trucks';
import { Truck } from '../../models/truck.model';
import { SelectDriverPage } from '../select-driver/select-driver';
import { AuthProvider } from '../../providers/auth/auth';


@IonicPage()
@Component({
  selector: 'page-truck-list',
  templateUrl: 'truck-list.html',
  providers: [AuthProvider]
})
export class TruckListPage {

  public trucks: Truck[]

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public truckService: TrucksProvider,
    public modalCtrl: ModalController,
    private auth: AuthProvider
  ) {
    this.getTrucksList()
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TruckListPage');
  }

  async getTrucksList() {
    const truckerId: number = <number> await this.auth.getTruckerId()
    console.log(truckerId);
    this.truckService.getAllTrucks(truckerId)
      .subscribe(
        trucks => { this.trucks = trucks },
        err => { console.log(err) }
      )
  }

  showAssignDriverModal(truckId: number) {
    const selectedTruck: Truck = this.trucks.find(truck => truckId === truck.Id);
    this.modalCtrl.create(SelectDriverPage, {truck:selectedTruck}).present()
  }

}
