import { TruckType } from "./truck-type.model";
import { Type, Expose } from "class-transformer";

export class Truck {

  CreatedByUserId: number
  CreatedDate: string
  Height: number
  IsLockin: number
  Id: number
  LastUpdatedByUserId: number
  LastUpdatedDate: string
  Length:number
  PlateNo: string
  Status: boolean
  TruckTypeId: number
  TruckerId: number
  VolumeCapacity: number
  WeightCapacity: number
  Width: number

  @Type(() => TruckType)
  TruckType: TruckType

  @Expose()
  getTruckType() {
    return this.TruckType.Type
  }

  
}