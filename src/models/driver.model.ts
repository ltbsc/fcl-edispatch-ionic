import { Expose } from "class-transformer";

export class Driver {
  CreatedByUserId: number
  CreatedDate: string
  DriverContactPhones: Object[]
  FileName: string
  FilePath: string
  FirstName: string
  Id: number
  LastName: string
  LastUpdatedByUserId: number
  LastUpdatedDate: string
  LicenseExpiry: string
  LicenseNo: string
  MiddleName: string
  TruckerId: number

  @Expose()
  fullName() {
    return `${this.FirstName} ${this.LastName}`
  }
}