import { Expose } from "class-transformer";

export class User {

  ID: number
  FirstName: string
  LastName: string
  Midllename: string
  Email: string
  ImageName: string
  UserTypeId: number
  UserTypeName: string

  @Expose()
  getFullName() {
    return `${this.FirstName} ${this.Midllename} ${this.LastName}`
  }

}