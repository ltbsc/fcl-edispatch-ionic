import { Type } from "class-transformer";
import { Address } from "./address.model";

export class Trucker {

  Id: number
  Name: string
  TIN: string

  @Type(() => Address)
  Address: Address
}