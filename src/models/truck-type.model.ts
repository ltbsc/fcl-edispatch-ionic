export class TruckType {

  CreatedByUserId: number
  CreatedDate: string
  Id: number
  LastUpdatedByUserId: number
  LastUpdatedDate: string
  Type: string
  VolumeCapacity: number
  WeightCapacity: number
  
} 