export class API {

  /**
   * The server's base URL
   */
  private static readonly BASE_URL = 'http://devtms.fastgroup.biz/api/';


  /**
   *  
   * @param segment The URL segment to append
   * 
   */
  static baseURL (segment: string = '') : string {
    return `${this.BASE_URL}${segment}`;
  }

}